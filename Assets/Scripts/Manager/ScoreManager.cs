﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Manager
{
    public class ScoreManager : MonoSingleton<ScoreManager>
    {
        public static ScoreManager Instance { get; private set; }

        public int Score { get; private set; }

        [SerializeField] private TextMeshProUGUI scoreText;


        public void Init(GameManager gameManager)
        {
            GameManager.Instance.OnRestarted += OnRestarted;
        }

        private void Awake()
        {
            Debug.Assert(scoreText != null, "scoreText cannot null");

            if (Instance == null)
            {
                Instance = this;
            }

            DontDestroyOnLoad(this);
        }

        private void OnRestarted()
        {
            GameManager.Instance.OnRestarted -= OnRestarted;
        }
    }
}


